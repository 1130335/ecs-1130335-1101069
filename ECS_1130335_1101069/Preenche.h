/* 
 * File:   Preenche.h
 * Author: Rui
 *
 * Created on 25 de Outubro de 2014, 18:14
 */

#ifndef PREENCHE_H
#define	PREENCHE_H

#include "Armazem.h"

class Preenche{
private:
    Armazem arm;
    Deposito_Fresco* df;
    Deposito_Normal* dn;
    int dNmin, dNmax, dFmin, dFmax, pNmin, pNmax, pFmin, pFmax, dmin, dmax,cmin,cmax,amin,amax;
    int den,def,pn,pf;
public:
    Preenche();
    Preenche(const Preenche&);
    virtual ~Preenche();
    void setValores(int, int, int, int, int, int, int, int, int, int, int, int, int, int);
    int nAleatorio(int,int);
    friend ostream& operator<< (ostream& os, Preenche &p);
    void constroi();
    void process(char,int);
    void processMultiple(int, char, int);
};

Preenche::Preenche(){
    
}

Preenche::~Preenche(){
    
}

void Preenche::processMultiple(int dep,char opc, int qtd)
{
    string prod= "Produto Personalizado";
    cout << "opcao: " << opc << " inseridas/removidas "<< qtd <<" unidades do deposito "<<dep <<endl;
    int i,z,encontra=0;
    list<Deposito*> aux = arm.l_depositos;
    
    if(opc=='7')
    {
        for (int i = 0; (i < arm.tot)&&(encontra==0); i++) 
        {
            if((*(aux.front())).mostraID()==dep)
                if (typeid (*(aux.front())) == typeid (Deposito_Fresco))
                    for(z=0;z<qtd;z++)
                        dynamic_cast<Deposito_Fresco *> (aux.front())->removerProduto();
            aux.pop_front();
        }
    }else
        {
            if(opc=='8')
            {
                for (int i=0; (i<arm.tot) && (encontra==0);i++)
                {
                    if((*(aux.front())).mostraID()==dep)
                        if (typeid (*(aux.front())) == typeid (Deposito_Normal))
                            for(z=0;z<qtd;z++)
                                dynamic_cast<Deposito_Normal *> (aux.front())->removerProduto();
                    aux.pop_front();
                }
            }else
                {
                    if(opc=='5')
                    {
                        for (int i=0; (i<arm.tot) && (encontra==0);i++)
                        {
                            if((*(aux.front())).mostraID()==dep)
                                if (typeid (*(aux.front())) == typeid (Deposito_Normal))
                                    for(z=0;z<qtd;z++)
                                        dynamic_cast<Deposito_Normal *> (aux.front())->inserirProduto(prod);
                            aux.pop_front();    
                        }
                    }else
                        {
                            if(opc=='6')
                            {
                                for (int i=0; (i<arm.tot) && (encontra==0);i++)
                                {
                                    if((*(aux.front())).mostraID()==dep)
                                        if (typeid (*(aux.front())) == typeid (Deposito_Normal))
                                            for(z=0;z<qtd;z++)
                                                dynamic_cast<Deposito_Normal *> (aux.front())->inserirProduto(prod);
                                    aux.pop_front();
                                }
                            }
                            
                        }
                }
        }
    arm.escreve();
    arm.guardarFicheiro();
}

void Preenche::process(char opc, int qtd)
{
    string prod= "Produto Personalizado";
    int indice;
    if(opc=='1')
    {
        for(indice=1;indice<=qtd;indice++)
            (*df).inserirProduto(prod);
    }
    else{
        if(opc=='2')
        {
            for(indice=1;indice<=qtd;indice++)
                (*dn).inserirProduto(prod);
        }
        else{
            if(opc=='3')
            {
                for(indice=1;indice<=qtd;indice++)
                    (*df).removerProduto();
            }else
            {
                for(indice=1;indice<=qtd;indice++)
                    (*dn).removerProduto();
            }
        }
    }
    arm.escreve();
    arm.guardarFicheiro();
}

void Preenche::constroi()
{
    int i,z,d1=-1,d2=-1;
    for(i=0;i<def;i++){
        while(d1==-1)
        {
            d1=this->nAleatorio(1,def+den);
            if(d1==i+1)
                d1=-1;
        }
        while(d2==-1)
        {
            d2=this->nAleatorio(1,def+den);
            if((d2==i+1)||(d2==d1))
                d2=-1;
        }
        df = new Deposito_Fresco(i+1, pf, this->nAleatorio(cmax,cmin), this->nAleatorio(amin,amax),d1,this->nAleatorio(dmin,dmax),d2,this->nAleatorio(dmin,dmax));
        string p1 = "Produto 1";
        string p2 = "Produto 2";
        string p3 = "Produto 3";
        string p4 = "Produto 4";
        (*df).inserirProduto(p1);
        (*df).inserirProduto(p2);
        (*df).inserirProduto(p3);
        (*df).inserirProduto(p4);
        arm.inserirDeposito(df);
        d1=-1,d2=-1;
    }
    for(z=0;z<den;z++){
        d1=-1,d2=-1;
        while(d1==-1)
        {
            d1=this->nAleatorio(1,def+den);
            if(d1==i+1)
                d1=-1;
        }
        while(d2==-1)
        {
            d2=this->nAleatorio(1,def+den);
            if((d2==i+1)||(d2==d1))
                d2=-1;
        }   
        dn = new Deposito_Normal(i+1+z, pn, this->nAleatorio(cmax,cmin), this->nAleatorio(amin,amax),d1,this->nAleatorio(dmin,dmax),d2,this->nAleatorio(dmin,dmax));
        string p1 = "Produto 5";
        string p2 = "Produto 6";
        string p3 = "Produto 7";
        string p4 = "Produto 9";
        (*dn).inserirProduto(p1);
        (*dn).inserirProduto(p2);
        (*dn).inserirProduto(p3);
        (*dn).inserirProduto(p4);
        arm.inserirDeposito(dn);
        d1=-1,d2=-1;
    }
    arm.escreve();
    arm.guardarFicheiro();
}

void Preenche::setValores(int depNmin, int depNmax, int depFmin, int depFmax, int panNmin, int palNmax, int palFmin, int palFmax, int distmin, int distmax, int capmin, int capmax, int areamin, int areamax){
    dNmin=depNmin; dNmax=depNmax; dFmin=depFmin; dFmax=depFmax;
    pNmin=panNmin; pNmax=palNmax; pFmin=palFmin; pFmax=palFmax;
    dmin=distmin; dmax=distmax;
    cmin= capmin; cmax= capmax; amin = areamin; amax= areamax;
    den=this->nAleatorio(dNmin,dNmax);
    def=this->nAleatorio(dFmin,dFmax);
    pn=this->nAleatorio(pNmin,pNmax);
    pf=this->nAleatorio(pFmin,pFmax);
}
int Preenche::nAleatorio(int min, int max) {
    return ((rand() % (max - min)) + min);
}

ostream& operator<< (ostream& os, Preenche &p) 
{ 
 os << "Depositos normais: " << p.den << "; Depositos Frescos: " << p.def << "; Paletes Normais: "<<p.pn<< "; Paletes Frescos: "<<p.pf; 
 return os; 
}

#endif