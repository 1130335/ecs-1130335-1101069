#ifndef ARMAZEM_H
#define	ARMAZEM_H

#include "Deposito.h"
#include "Deposito_Fresco.h"
#include "Deposito_Normal.h"
#include <sstream>


#include "graphStlPath.h"

class Armazem {
private:
    list<string> lfresco,lnormal;
    graphStlPath <string, int> g;
    int n_dFrescos; // número de depósitos frescos no armazém
    int n_dNormais; // número de depósitos normais no armazém
    double m_distancia; // distância entre de depósitos

public:
    list<Deposito*> l_depositos; // lista com apontadores para depósitos
    int tot;
    Armazem(); // construtos sem parâmetros do armazém
    ~Armazem(); // destrutor
    void setDistancia(double); // modifica a distância entre depósitos
    double getDistancia(); // permite visualizar a distância entre depósitos
    void escreve(); // mostra para o ecrã a estrutura completa do armazém
    void guardarFicheiro(); // guarda a estrutura do armazém num ficheiro de texto
    void inserirDeposito(Deposito*); // insere um novo depósito ao armazém
    void removerDeposito(); // remove um depósito do armazém
    void setValG();
    void caminhoMin(string,string);
    void caminhosDistintos(string,string);
    void caminhoEspecial(string,string,int);
    int verifica(stack <string>, list<string>,string,string);
};

int atoi(const string& str) {
  int n = 0;
  for (int i = 0; i < str.size(); i += 1) {
    char digit = str.at(i); 
    if (digit < '0' || digit > '9') {
      return n;
    }
    n *= 10;
    n += digit - '0';
  }
  return n;
}

int Armazem::verifica(stack <string>  a, list<string> li, string p,string s)
{
    stack <string>  aux2;
    while (!a.empty()) {
        if((a.top().compare(p)!=0)&&(a.top().compare(s)!=0))
        {
            int enc=0;
            for (list<string>::iterator it = li.begin(); (it != li.end()&&enc==0); it++)
            {
                if(a.top().compare(*it)==0)
                {
                    aux2.push(a.top());
                    enc=1;
                }
            }
            if(enc==0)
                return 0;
        }
        else
        {
            aux2.push(a.top());
        }
        a.pop(); 
    }
    while(!aux2.empty())
    {
        cout<<aux2.top()<<"->";
        aux2.pop();
    }
    cout<<"END"<<endl;;
    return 1;
}

Armazem::Armazem() {
    n_dFrescos = 0;
    m_distancia = 0;
    n_dNormais = 0;
}

Armazem::~Armazem() {

}

void Armazem::setValG()
{   //0-inicio 1-destino1 2-distDest1 3-dest2 4-distDest2
    string line;
    ifstream myfile("Distancias.txt");
    if (myfile.is_open()) {
        int z=1,laux;
        getline(myfile, line);
        laux=atoi(line);// numero de ligaçoes
        for(z=1;z<=laux;z++){
            getline(myfile, line);
            string arr[5];
            int i = 0;
            stringstream ssin(line);
            while (ssin.good() && i < 5){
                ssin >> arr[i];
                ++i;
            }
            g.addGraphEdge(atoi(arr[2]), arr[0], arr[1]); //(dist,origem,destino)
            g.addGraphEdge(atoi(arr[4]), arr[0], arr[3]);
        }
        getline(myfile, line);
        laux=atoi(line);// numero de depositos normais
        for(z=1;z<=laux;z++){
            getline(myfile, line);
            lnormal.push_front(line);
        } 
        getline(myfile, line);
        laux=atoi(line);// numero de depositos frescos
        for(z=1;z<=laux;z++){
            getline(myfile, line);
        }
        myfile.close();
        cout << "Lido ficheiro com depositos" << endl;
    }
    else cout << "Nao é possível abrir o ficheiro";
}


void Armazem::caminhosDistintos(string ini, string fim)
{
    queue < stack <string> > q;
    stack <string>  aux,aux2;
    q=g.distinctPaths(ini, fim);
    while (!q.empty()) {
        aux=q.front();
        while (!aux.empty()) {
            aux2.push(aux.top());
            aux.pop(); 
        }
        while(!aux2.empty())
        {
            cout<<aux2.top()<<"->";
            aux2.pop();
        }
        cout<<"END"<<endl;
        q.pop();
    }
}

void Armazem::caminhoEspecial(string ini, string fim, int opc)
{
    queue < stack <string> > q;
    stack <string>  aux;
    q=g.distinctPaths(ini, fim);
    int encontra=0;
    if(opc==0)//normais
    {
        while (!q.empty()&&encontra==0)
        {
            aux=q.front();
            encontra=verifica(aux,lnormal,ini,fim);
            q.pop();
        }
    }else{
        if(opc==1)//frescos
        {
            while (!q.empty()&&encontra==0)
            {
                aux=q.front();
                encontra=verifica(aux,lfresco,ini,fim);
                q.pop();
            }
        }
    }
    if(encontra==0)
        cout<<"nao foi possivel encontrar um caminho válido"<<endl;
}

void Armazem::caminhoMin(string pri, string seg)
{
    stack <string>  aux;
    vector<int> path, dist;
    g.dijkstrasAlgorithm(pri, path, dist);
    cout<<"\nCaminho Minimo de "<<pri<<" para "<<seg<<": "<<endl;
    for(int i =0;i<path.size();i++) {
        if(path[i]!=-1) {
            string vert,teste="";
            //testar veracidade do valor
            g.getVertexContentByKey(vert,i);
            if (vert==seg)
            {
                int vi = i;
                while(vi!=-1) {
                    g.getVertexContentByKey(vert,vi);
                    aux.push(vert);
                    teste="";
                    if(path[vi]!=-1) {
                        int e;
                        g.getEdgeByVertexKeys(e,path[vi],vi);
                        teste+="->(";
                        stringstream ss;
                        ss<<e;
                        teste+=ss.str();
                        teste+=")->";
                        aux.push(teste);
                    }
                    vi=path[vi];
                }
            }
        }
    }
    while(!aux.empty())
    {
        cout<<aux.top();
        aux.pop();
    }
    cout<<endl;
}

void Armazem::setDistancia(double d) {
    m_distancia = d;
}

void Armazem::inserirDeposito(Deposito* d) {
    if (l_depositos.empty()) {
        l_depositos.push_back(d);
        if (typeid (*d) == typeid (Deposito_Fresco)) {
            n_dFrescos++;
        } else {
            n_dNormais++;
        }
    } else {
        l_depositos.push_back(d);

        if (typeid (*d) == typeid (Deposito_Normal)) {
            n_dNormais++;
        } else {
            n_dFrescos++;
        }
    }
}

void Armazem::removerDeposito() {
    if (l_depositos.size() != 0) {
        if (typeid (l_depositos.back()) == typeid (Deposito_Fresco)) {
            n_dFrescos--;
        } else {

            n_dNormais--;
        }
        l_depositos.pop_back();
    }
}

void Armazem::escreve() {
    cout << "ARMAZEM" << endl;
    cout << "Nº de Depositos Frescos: " << n_dFrescos << endl;
    cout << "Nº de Depositos Normais: " << n_dNormais << endl;
    cout << endl;
    this->tot=this->n_dFrescos+this->n_dNormais;
    list<Deposito*> aux = l_depositos;
    for (int i = 0; i < (n_dFrescos + n_dNormais); i++) {
        if (typeid (*(aux.front())) == typeid (Deposito_Fresco)) {
            cout << "DEPOSITO Nº " << i + 1;
            dynamic_cast<Deposito_Fresco *> (aux.front())->escreve();
            aux.pop_front();
        } else if (typeid (*(aux.front())) == typeid (Deposito_Normal)) {
            cout << "DEPOSITO Nº " << i + 1;
            dynamic_cast<Deposito_Normal *> (aux.front())->escreve();
            aux.pop_front();
        }
    }
}

void Armazem::guardarFicheiro() {
    ofstream doc("Farmazem.txt");
    ofstream doc2("Distancias.txt");
    doc2 << "teste"<<endl;
    doc <<"Numero de depósitos Frescos: " << n_dFrescos << endl;
    doc <<"Numero de depósitos Normais: "<< n_dNormais << endl<<endl;
    doc << endl;
    list<Deposito*> aux = l_depositos;
    for (int i = 0; i < (n_dFrescos + n_dNormais); i++) {
        aux.front()->guardarFicheiroDist(doc2);
        if (typeid (*(aux.front())) == typeid (Deposito_Fresco)) {
            doc << "DEPOSITO Nº " << i + 1;
            dynamic_cast<Deposito_Fresco *> (aux.front())->guardarFicheiro(doc);
            aux.pop_front();
        } else if (typeid (*(aux.front())) == typeid (Deposito_Normal)) {
            doc << "DEPOSITO Nº " << i + 1;
            dynamic_cast<Deposito_Normal *> (aux.front())->guardarFicheiro(doc);
            aux.pop_front();
        }
    }
    doc2.close();
    doc.close();
}


#endif	/* ARMAZEM_H */

