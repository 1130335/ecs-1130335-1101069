#ifndef PALETENORMAL_H
#define	PALETENORMAL_H

class PaleteNormal {
private:
    stack<string> m_palete; // palete em pilha
    int m_atual; // número de produtos atuais
    int m_capacidade; // capacidade máxima da palete

public:
    PaleteNormal(int); // construtor que recebe o tamanho máximo da palete como parâmetro
    PaleteNormal(const PaleteNormal&); // construtor que recebe uma palete como parâmetro
    ~PaleteNormal(); // destrutor
    void inserirProduto(string); // insere um produto na palete (assume-se que o produto é do tipo string)
    void removerProduto(); // remove o primeiro elemento da palete
    void escreve(); // apresenta no ecrã todos os produtos na palete
    void setCapacidade(int); // altera a capacidade máxima da palete
    int getCapacidade(); // visualiza a capacidade máxima da palete
    int getAtual(); // visualiza o número de produtos atuais na palete
    void guardarFicheiro(ofstream&); // guarda a estrutura da palete num ficheiro de texto
};

PaleteNormal::PaleteNormal(int tamanho) {
    m_capacidade = tamanho;
    m_atual = 0;
}

PaleteNormal::PaleteNormal(const PaleteNormal& pn) {
    this->m_palete = pn.m_palete;
    this->m_capacidade = pn.m_capacidade;
    this->m_atual = pn.m_atual;
}

PaleteNormal::~PaleteNormal() {

}

void PaleteNormal::inserirProduto(string produto) {
    if (m_atual < m_capacidade) {
        m_palete.push(produto);
        m_atual++;
    } else {
        cout << "Erro: palete cheia..." << endl << endl;
    }
}

void PaleteNormal::removerProduto() {
    if (m_atual == 0) {
        cout << "Erro: palete vazia..." << endl << endl;
    } else {
        m_palete.pop();
        m_atual--;
    }
}

void PaleteNormal::escreve() {
    stack <string> aux = m_palete;
    if (m_atual == 0) {
        cout << "vazia" << endl;
    } else {
        for (int i = 0; i < m_atual; i++) {
            cout << aux.top() << endl;
            aux.pop();
        }
    }
}

void PaleteNormal::setCapacidade(int cap) {
    m_capacidade = cap;
}

int PaleteNormal::getCapacidade() {
    return m_capacidade;
}

int PaleteNormal::getAtual() {
    return m_atual;
}

void PaleteNormal::guardarFicheiro(ofstream& doc) {
    stack <string> aux = m_palete;
    if (m_atual == 0) {
        doc << "vazia" << endl;
    } else {
        for (int i = 0; i < m_atual; i++) {
            doc << aux.top() << endl;
            aux.pop();
        }
    }
}

#endif	/* PALETENORMAL_H */

