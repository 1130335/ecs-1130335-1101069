#ifndef DEPOSITO_H
#define	DEPOSITO_H

class Deposito {
private:
    int m_chave; // código do depósito
    int m_paletes; // número de paletes no depósito
    int m_capacidade; // capacidade máxima das paletes do depósito
    double m_area; // área do depósito
    int m_dep1;
    int m_dist1;
    int m_dep2;
    int m_dist2;

public:
    Deposito(); // construtor por defeito do depósito
    Deposito(int, int, int, double, int, int, int, int); // construtor com todos os parâmetros do depósito
    Deposito(const Deposito&); // construtor por cópia do depósito
    virtual ~Deposito(); // destrutor
    void escreve(); // mostra para o ecrã os atributos do depósito
    int getChave(); // visualiza o código do depósito
    int getPaletes(); // visualiza o número de paletes no depósito
    int getCapacidade(); // visualiza a capacidade máxima das paletes
    double getArea(); // visualiza a área do depósito
    void setChave(int); // modifica o código do depósito
    void setCapacidade(int); // modifica a capacidade máxima das paletes
    void setArea(double); // modifica a área do depósito
    void guardarFicheiro(ofstream&); // guarda a estrutura do depósito num ficheiro de texto
    void guardarFicheiroDist(ofstream&);
    int mostraID();
};

int Deposito::mostraID()
{
    return m_chave;
}

Deposito::Deposito() {
    m_chave = 0;
    m_paletes = 1;
    m_capacidade = 1;
    m_area = 0.0;
    m_dep1=0;
    m_dist1=0;
    m_dep2=0;
    m_dist2=0;
}

Deposito::Deposito(int ch, int pal, int cap, double area, int dep1, int dist1, int dep2, int dist2) {
    m_chave = ch;
    m_paletes = pal;
    m_capacidade = cap;
    m_area = area;
    m_dep1=dep1;
    m_dist1=dist1;
    m_dep2=dep2;
    m_dist2=dist2;      
}

Deposito::Deposito(const Deposito& d) {
    m_chave = d.m_chave;
    m_paletes = d.m_paletes;
    m_capacidade = d.m_capacidade;
    m_area = d.m_area;
    m_dep1=d.m_dep1;
    m_dist1=d.m_dist1;
    m_dep2=d.m_dep2;
    m_dist2=d.m_dist2;    
}

Deposito::~Deposito() {

}

int Deposito::getChave() {
    return m_chave;
}

int Deposito::getPaletes() {
    return m_paletes;
}

int Deposito::getCapacidade() {
    return m_capacidade;
}

double Deposito::getArea() {
    return m_area;
}

void Deposito::setChave(int ch) {
    m_chave = ch;
}

void Deposito::setCapacidade(int cap) {
    m_capacidade = cap;
}

void Deposito::setArea(double area) {
    m_area = area;
}

void Deposito::escreve() {
    cout << "Chave = " << m_chave << endl;
    cout << "Nº Total de Paletes = " << m_paletes << endl;
    cout << "Area = " << m_area << " metros quadrados" << endl;
    cout << "capacidade das paletes "<< m_capacidade << endl;
    cout << "caminho ao primeiro deposito "<< m_dep1 << endl;
    cout << "distancia ao primeiro deposito "<< m_dist1 << endl;
    cout << "caminho ao segundo deposito "<< m_dep2 << endl;
    cout << "distancia ao segundo deposito "<< m_dist2 <<endl; 
    cout << endl;
}

void Deposito::guardarFicheiro(ofstream& doc) {
    doc << "chave "<< m_chave << endl;
    doc << "numero paletes "<< m_paletes << endl;
    doc << "area "<< m_area <<  endl;
    doc << "capacidade das paletes "<< m_capacidade << endl;
    doc << "caminho ao primeiro deposito "<< m_dep1 << endl;
    doc << "distancia ao primeiro deposito "<< m_dist1 << endl;
    doc << "caminho ao segundo deposito "<< m_dep2 << endl;
    doc << "distancia ao segundo deposito "<< m_dist2 <<endl; 
    doc << endl;
}

void Deposito::guardarFicheiroDist(ofstream& doc2) {
    doc2 << m_chave << ";" << m_dep1 << ";" << m_dist1 << ";" << m_dep2 << ";" << m_dist2 << endl;
}

#endif	/* DEPOSITO_H */

