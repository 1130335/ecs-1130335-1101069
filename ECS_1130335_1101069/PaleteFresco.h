#ifndef PALETEFRESCO_H
#define	PALETEFRESCO_H

class PaleteFresco {
private:
    queue<string> m_palete; // palete com ordem de chegada
    int m_atual; // número de produtos atuais
    int m_capacidade; // capacidade máxima da palete
public:
    PaleteFresco(int); // construtor que recebe o tamanho máximo da palete como parâmetro
    PaleteFresco(const PaleteFresco&); // construtor que recebe uma palete como parâmetro
    ~PaleteFresco(); // destrutor
    void inserirProduto(string); // insere um produto na palete (assume-se que o produto é do tipo string)
    void removerProduto(); // remove o primeiro elemento da palete
    void escreve(); // apresenta no ecrã todos os produtos na palete
    void setCapacidade(int); // altera a capacidade máxima da palete
    int getCapacidade(); // visualiza a capacidade máxima da palete
    int getAtual(); // visualiza o número de produtos atuais na palete
    void guardarFicheiro(ofstream&); // guarda a estrutura da palete num ficheiro de texto
};

PaleteFresco::PaleteFresco(int tamanho) {
    m_capacidade = tamanho;
    m_atual = 0;
}

PaleteFresco::PaleteFresco(const PaleteFresco& pf) {
    this->m_palete = pf.m_palete;
    this->m_capacidade = pf.m_capacidade;
    this->m_atual = pf.m_atual;
}

PaleteFresco::~PaleteFresco() {

}

void PaleteFresco::inserirProduto(string produto) {
    if (m_atual < m_capacidade) {
        m_palete.push(produto);
        m_atual++;
    } else {
        cout << "Erro: palete cheia..." << endl << endl;
    }
}

void PaleteFresco::removerProduto() {
    if (m_atual == 0) {
        cout << "Erro: palete vazia..." << endl << endl;
    } else {
        m_palete.pop();
        m_atual--;
    }
}

void PaleteFresco::escreve() {
    queue <string> aux = m_palete;
    if (m_atual == 0) {
        cout << "vazia" << endl;
    } else {
        for (int i = 0; i < m_atual; i++) {
            cout << aux.front() << endl;
            aux.pop();
        }
    }
}

void PaleteFresco::setCapacidade(int cap) {
    m_capacidade = cap;
}

int PaleteFresco::getCapacidade() {
    return m_capacidade;
}

int PaleteFresco::getAtual() {
    return m_atual;
}

void PaleteFresco::guardarFicheiro(ofstream& doc) {
    queue <string> aux = m_palete;
    if (m_atual == 0) {
        doc << "vazia" << endl;
    } else {
        for (int i = 0; i < m_atual; i++) {
            doc << aux.front() << endl;
            aux.pop();
        }
    }
}

#endif	/* PALETEFRESCO_H */

