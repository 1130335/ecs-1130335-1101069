Numero de depósitos Frescos: 8
Numero de depósitos Normais: 9


DEPOSITO Nº 1 - FRESCO
chave 1
numero paletes 5
area 19
capacidade das paletes 4
caminho ao primeiro deposito 3
distancia ao primeiro deposito 14
caminho ao segundo deposito 5
distancia ao segundo deposito 10

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 2 - FRESCO
chave 2
numero paletes 5
area 17
capacidade das paletes 6
caminho ao primeiro deposito 8
distancia ao primeiro deposito 16
caminho ao segundo deposito 1
distancia ao segundo deposito 15

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 3 - FRESCO
chave 3
numero paletes 5
area 19
capacidade das paletes 5
caminho ao primeiro deposito 8
distancia ao primeiro deposito 15
caminho ao segundo deposito 7
distancia ao segundo deposito 17

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 4 - FRESCO
chave 4
numero paletes 5
area 11
capacidade das paletes 6
caminho ao primeiro deposito 10
distancia ao primeiro deposito 15
caminho ao segundo deposito 3
distancia ao segundo deposito 18

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 5 - FRESCO
chave 5
numero paletes 5
area 10
capacidade das paletes 4
caminho ao primeiro deposito 8
distancia ao primeiro deposito 18
caminho ao segundo deposito 2
distancia ao segundo deposito 14

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 6 - FRESCO
chave 6
numero paletes 5
area 11
capacidade das paletes 5
caminho ao primeiro deposito 11
distancia ao primeiro deposito 12
caminho ao segundo deposito 2
distancia ao segundo deposito 15

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 7 - FRESCO
chave 7
numero paletes 5
area 13
capacidade das paletes 4
caminho ao primeiro deposito 12
distancia ao primeiro deposito 15
caminho ao segundo deposito 4
distancia ao segundo deposito 10

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 8 - FRESCO
chave 8
numero paletes 5
area 15
capacidade das paletes 6
caminho ao primeiro deposito 6
distancia ao primeiro deposito 19
caminho ao segundo deposito 10
distancia ao segundo deposito 12

PALETE 1
Produto 1

PALETE 2
Produto 2

PALETE 3
Produto 3

PALETE 4
Produto 4

PALETE 5
vazia

DEPOSITO Nº 9 - NORMAL
chave 9
numero paletes 3
area 10
capacidade das paletes 6
caminho ao primeiro deposito 16
distancia ao primeiro deposito 19
caminho ao segundo deposito 10
distancia ao segundo deposito 18

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 10 - NORMAL
chave 10
numero paletes 3
area 13
capacidade das paletes 4
caminho ao primeiro deposito 11
distancia ao primeiro deposito 11
caminho ao segundo deposito 16
distancia ao segundo deposito 12

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 11 - NORMAL
chave 11
numero paletes 3
area 11
capacidade das paletes 6
caminho ao primeiro deposito 16
distancia ao primeiro deposito 12
caminho ao segundo deposito 15
distancia ao segundo deposito 19

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 12 - NORMAL
chave 12
numero paletes 3
area 16
capacidade das paletes 5
caminho ao primeiro deposito 6
distancia ao primeiro deposito 14
caminho ao segundo deposito 8
distancia ao segundo deposito 18

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 13 - NORMAL
chave 13
numero paletes 3
area 12
capacidade das paletes 4
caminho ao primeiro deposito 7
distancia ao primeiro deposito 16
caminho ao segundo deposito 1
distancia ao segundo deposito 14

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 14 - NORMAL
chave 14
numero paletes 3
area 10
capacidade das paletes 5
caminho ao primeiro deposito 13
distancia ao primeiro deposito 17
caminho ao segundo deposito 6
distancia ao segundo deposito 15

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 15 - NORMAL
chave 15
numero paletes 3
area 18
capacidade das paletes 4
caminho ao primeiro deposito 16
distancia ao primeiro deposito 11
caminho ao segundo deposito 8
distancia ao segundo deposito 11

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 16 - NORMAL
chave 16
numero paletes 3
area 15
capacidade das paletes 4
caminho ao primeiro deposito 3
distancia ao primeiro deposito 18
caminho ao segundo deposito 15
distancia ao segundo deposito 12

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

DEPOSITO Nº 17 - NORMAL
chave 17
numero paletes 3
area 18
capacidade das paletes 4
caminho ao primeiro deposito 10
distancia ao primeiro deposito 12
caminho ao segundo deposito 12
distancia ao segundo deposito 17

PALETE 1
Produto 9
Produto 7
Produto 6
Produto 5

PALETE 2
vazia

PALETE 3
vazia

