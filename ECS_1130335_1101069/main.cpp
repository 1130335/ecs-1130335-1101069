#include <queue>
#include <stack>
#include <vector>
#include <iterator>
#include <list>
#include <string>
#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <typeinfo>
#include <cstring>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

#include "Armazem.h"
#include "Preenche.h"

Preenche valores;

void recolhedados()
{
    /*int depNmin,depNmax,depFmin,depFmax,palNmin,palNmax,palFmin,palFmax,distmin,distmax,capmin,capmax,areamin,areamax;
    cout<<"numero minimo de depositos normais?"<<endl;
    cin >> depNmin;
    cout<<"numero maximo de depositos normais?"<<endl;
    cin >>depNmax;
    cout<<"numero minimo de depositos frescos?"<<endl;
    cin >>depFmin;
    cout<<"numero maximo de depositos frescos?"<<endl;
    cin >>depFmax;
    cout<<"numero minimo de paletes de depositos normais?"<<endl;
    cin >>palNmin;
    cout<<"numero maximo de paletes de depositos normais?"<<endl;
    cin >>palNmax;
    cout<<"numero minimo de paletes de depositos frescos?"<<endl;
    cin >>palFmin;
    cout<<"numero maximo de paletes de depositos frescos?"<<endl;
    cin >>palFmax;
    cout<<"numero minimo de distancia entre depositos?"<<endl;
    cin >>distmin;
    cout<<"numero maximo de distancia entre depositos?"<<endl;
    cin >>distmax;
    cout<<"numero minimo de capacidade de depositos?"<<endl;
    cin >>capmin;
    cout<<"numero maximo de distancia de depositos?"<<endl;
    cin >>capmax;
    cout<<"numero minimo de area de depositos?"<<endl;
    cin >>areamin;
    cout<<"numero mmaximo de area de depositos?"<<endl;
    cin >>areamax;
    valores.setValores(depNmin,depNmax,depFmin,depFmax,palNmin,palNmax,palFmin,palFmax,distmin,distmax,capmin,capmax,areamin,areamax);*/
    valores.setValores(2,15,2,10,3,6,3,6,10,20,1,4,10,20);
    valores.constroi();
    cout << valores << endl;
}

int main(int argc, char** argv) {
    srand(time(NULL));
    //recolhedados();
    char op='9';
    int qtd,dep;
    Armazem a;
    while(op!='0')
    {
        cin.clear();
        cout<<"1-Inserir produtos frescos"<<endl;
        cout<<"2-Inserir produtos normais"<<endl;
        cout<<"3-Remover produtos frescos"<<endl;
        cout<<"4-Remover produtos normais"<<endl;
        cout<<"5-Inserir produtos frescos em determinado deposito"<<endl;
        cout<<"6-Inserir produtos normais em determinado deposito"<<endl;
        cout<<"7-Remover produtos frescos em determinado deposito"<<endl;
        cout<<"8-Remover produtos normais em determinado deposito"<<endl;
        cout<<"a-Apresenta grafo construido"<<endl;
        cout<<"b-Apresenta percursos entre dois depositos"<<endl;
        cout<<"c-Apresenta percursos passando por um tipo de deposito"<<endl;
        cout<<"d-Apresenta caminho minimo entre dois depositos"<<endl;
        cout<<endl;
        op=cin.get();
        if((op=='1')||(op=='2')||(op=='3')||(op=='4'))
        {
            cout<<"opcao "<< op << " quantidade? "<<endl;
            cin >> qtd;
            cout<<endl;
            valores.process(op,qtd);
        }
        if((op=='5')||(op=='6')||(op=='7')||(op=='8'))
        {
            cout<<"opcao "<<op<< " quantidade? "<<endl;
            cin >> qtd;
            cout << "deposito? "<<endl;
            cin >> dep;
            valores.processMultiple(dep,op,qtd);
        }
        a.setValG();
        if(op=='a')
            a.setValG();
        if(op=='b')
        {
            string p="6",s="9";
            /*cout<<"Qual o primeiro deposito?"<<endl;
            cin>>p;
            cout<<"Qual o segundo deposito?"<<endl;
            cin>>s;*/
            a.caminhosDistintos(p,s);
        }
        if(op=='c')
        {
            string p="6",s="9";
            int opcao=0;
            /*cout<<"Qual o primeiro deposito?"<<endl;
            cin>>p;
            cout<<"Qual o segundo deposito?"<<endl;
            cin>>s;
            cout<<"Qual a opçao? (0-depositos normais 1-depositos frescos"<<endl;
            cin>>opcao;*/
            a.caminhoEspecial(p,s,opcao);
        }
        if(op=='d')
        {
            string p="6",s="9";
            /*cout<<"Qual o primeiro deposito?"<<endl;
            cin>>p;
            cout<<"Qual o segundo deposito?"<<endl;
            cin>>s;*/
            a.caminhoMin(p,s);
        }
        cin.clear();
    }
    return 0;
}

