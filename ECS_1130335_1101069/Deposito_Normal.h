#ifndef DEPOSITO_NORMAL_H
#define	DEPOSITO_NORMAL_H

#include "Deposito.h"
#include "PaleteNormal.h"

class Deposito_Normal : public Deposito {
private:
    vector<PaleteNormal> m_deposito; // vetor com todas as paletes do depósito
    int m_atI; // número atual da palete onde são inseridos produtos
    vector<PaleteNormal> ::iterator m_itI; // iterador para a inserção de produtos
    int m_atR; // número atual da palete onde são removidos produtos
    vector<PaleteNormal> ::iterator m_itR; // iterador para a remoção de produtos

public:
    Deposito_Normal(); // construtor por defeito do depósito normal
    Deposito_Normal(int, int, int, double, int, int, int, int); // construtor com todos os parâmetros do depósito normal
    Deposito_Normal(const Deposito_Normal&); // construtor por cópia do depósito normal
    ~Deposito_Normal(); // destrutor
    void setCapacidade(int); // capacidade máxima de produtos por palete
    void inserirProduto(string); // insere um produto numa palete
    void removerProduto(); // remove um produto numa palete
    void escreve(); // mostra para o ecrã a estrutura do depósito normal
    void guardarFicheiro(ofstream&); // guarda a estrutura do depósito num ficheiro de texto

};

Deposito_Normal::Deposito_Normal() : Deposito() {
    m_deposito.push_back(PaleteNormal(2 * Deposito::getCapacidade()));
    m_atI = 0;
    m_itI = m_deposito.begin();
    m_atR = 1;
    m_itR = m_deposito.begin() + 1;
}

Deposito_Normal::Deposito_Normal(int chave, int palete, int capacidade, double area,  int dep1, int dist1, int dep2, int dist2) : Deposito(chave, palete, capacidade, area, dep1, dist1, dep2, dist2) {
    m_deposito.push_back(PaleteNormal(2 * Deposito::getCapacidade()));
    for (int i = 1; i < Deposito::getPaletes(); i++) {
        if (i % 2 == 0) {
            m_deposito.push_back(PaleteNormal(2 * Deposito::getCapacidade()));
        } else {
            m_deposito.push_back(PaleteNormal(Deposito::getCapacidade()));
        }
    }
    m_atI = 0;
    m_itI = m_deposito.begin();
    m_atR = 1;
    m_itR = m_deposito.begin() + 1;
}

Deposito_Normal::Deposito_Normal(const Deposito_Normal& dp) : Deposito(dp) {
    this->m_deposito = dp.m_deposito;
    this->m_atI = dp.m_atI;
    this->m_itI = dp.m_itI;
    this->m_atR = dp.m_atR;
    this->m_itR = dp.m_itR;
}

Deposito_Normal::~Deposito_Normal() {

}

void Deposito_Normal::setCapacidade(int cap) {
    vector<PaleteNormal> ::iterator it;
    Deposito::setCapacidade(cap);
    int i = 0;
    for (it = m_deposito.begin(); it < m_deposito.end(); it++) {
        if (i % 2 == 0) {
            (*it).setCapacidade(2 * Deposito::getCapacidade());
        } else {
            (*it).setCapacidade(Deposito::getCapacidade());
        }
    }
}

void Deposito_Normal::inserirProduto(string produto) {
    if (m_atI % 2 == 0) {
        if (m_atI < Deposito::getPaletes()) {
            if ((*m_itI).getAtual()<(*m_itI).getCapacidade()) {
                (*m_itI).inserirProduto(produto);
            } else {
                m_atI++;
                m_atI++;
                m_itI++;
                m_itI++;
                if (m_atI < Deposito::getPaletes()) {
                    (*m_itI).inserirProduto(produto);
                } else {
                    m_atI = 1;
                    m_itI = m_deposito.begin() + 1;
                    if ((*m_itI).getAtual() == (*m_itI).getCapacidade()) {
                        cout << "Erro: deposito cheio..." << endl;
                    }
                }
            }
        }
    }
    if (m_atI % 2 != 0) {
        if (m_atI < Deposito::getPaletes()) {
            if ((*m_itI).getAtual()<(*m_itI).getCapacidade()) {
                (*m_itI).inserirProduto(produto);
            } else {
                m_atI++;
                m_atI++;
                m_itI++;
                m_itI++;
                if (m_atI < Deposito::getPaletes()) {
                    (*m_itI).inserirProduto(produto);
                } else {
                    m_atI = 0;
                    m_itI = m_deposito.begin();
                    if ((*m_itI).getAtual() == (*m_itI).getCapacidade()) {
                        cout << "Erro: deposito cheio..." << endl;
                    }
                }
            }
        }
    }
}

void Deposito_Normal::removerProduto() {
    if (m_atI % 2 != 0) {
        if (m_atI < Deposito::getPaletes()) {
            if ((*m_itI).getAtual() > 0) {
                (*m_itI).removerProduto();
            } else {
                m_atI++;
                m_atI++;
                m_itI++;
                m_itI++;
                if (m_atI < Deposito::getPaletes()) {
                    (*m_itI).removerProduto();
                } else {
                    m_atI = 0;
                    m_itI = m_deposito.begin();
                    if ((*m_itI).getAtual() == 0) {
                        cout << "Erro: deposito vazia..." << endl;
                    }
                }
            }
        }
    }

    if (m_atI % 2 == 0) {
        if (m_atI < Deposito::getPaletes()) {
            if ((*m_itI).getAtual() > 0) {
                (*m_itI).removerProduto();
            } else {
                m_atI++;
                m_atI++;
                m_itI++;
                m_itI++;
                if (m_atI < Deposito::getPaletes()) {
                    (*m_itI).removerProduto();
                } else {
                    m_atI = 1;
                    m_itI = m_deposito.begin() + 1;
                    if ((*m_itI).getAtual() == 0) {
                        cout << "Erro: deposito vazio..." << endl;
                    }
                }
            }
        }
    }
}

void Deposito_Normal::escreve() {
    cout << " - NORMAL" << endl;
    Deposito::escreve();
    int cont = 1;
    vector<PaleteNormal> aux = m_deposito;
    vector<PaleteNormal> ::iterator it;
    if (Deposito::getPaletes() == 0) {
        cout << "vazio" << endl;
    } else {
        for (it = aux.begin(); it != aux.end(); it++) {
            cout << "PALETE " << cont << endl;
            (*it).escreve();
            cont++;
            cout << endl;
        }
    }
}

void Deposito_Normal::guardarFicheiro(ofstream& doc) {
    doc << " - NORMAL" << endl;
    Deposito::guardarFicheiro(doc);
    int cont = 1;
    vector<PaleteNormal> aux = m_deposito;
    vector<PaleteNormal> ::iterator it;
    if (Deposito::getPaletes() == 0) {
        doc << "vazio" << endl;
    } else {
        for (it = aux.begin(); it != aux.end(); it++) {
            doc << "PALETE " << cont << endl;
            (*it).guardarFicheiro(doc);
            cont++;
            doc << endl;
        }
    }
}

#endif	/* DEPOSITO_NORMAL_H */

