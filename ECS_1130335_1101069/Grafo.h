/* 
 * File:   Grafo.h
 * Author: Rui
 *
 * Created on 5 de Novembro de 2014, 2:18
 */

#ifndef GRAFO_H
#define	GRAFO_H

#include <sstream>


#include "graphStlPath.h"

class Grafo{
private:
    graphStlPath <string, int> g;
public:
    Grafo();
    Grafo(const Grafo&);
    virtual ~Grafo();
    void setVal();
    void caminhoMin(char,char);
};

Grafo::Grafo() 
{ 
}

Grafo::~Grafo()
{    
}


int atoi(const string& str) {
  int n = 0;
  for (int i = 0; i < str.size(); i += 1) {
    char digit = str.at(i);   /* Could probably use iterator here,
                               * but this is more explicit. */
    if (digit < '0' || digit > '9') {
      return n;  /* No valid conversion possible. */
    }
    n *= 10;
    n += digit - '0';
  }
  return n;
}

void Grafo::setValG()
{   //0-inicio 1-destino1 2-distDest1 3-dest2 4-distDest2
    string line;
    ifstream myfile("Distancias.txt");
    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            string arr[5];
            int i = 0;
            stringstream ssin(line);
            while (ssin.good() && i < 5){
                ssin >> arr[i];
                ++i;
            }
            g.addGraphEdge(atoi(arr[2]), arr[0], arr[1]); //(dist,origem,destino)
            g.addGraphEdge(atoi(arr[4]), arr[0], arr[3]);
        }
        myfile.close();
        cout << "Lido ficheiro com depositos" << endl;
    }
    else cout << "Nao é possível abrir o ficheiro";
}

void caminhoMin(char pri, char seg)
{
    graphStlPath <char, int> g;
    vector<int> path, dist;
    g.dijkstrasAlgorithm('A', path, dist);
    cout<<"\nCaminho Minimo de A para: "<<endl;
    for(int i =0;i<path.size();i++) {
        if(path[i]!=-1) {
            char vert;
            g.getVertexContentByKey(vert,i);
            cout<<" - "<<vert<<" : ";
            int vi = i;
            while(vi!=-1) {
                g.getVertexContentByKey(vert,vi);
                cout<<vert;
                if(path[vi]!=-1) {
                    int e;
                    g.getEdgeByVertexKeys(e,path[vi],vi);
                    cout<<" ->("<<e<<")-";
                }
                vi=path[vi];
            }
            cout<<endl;
        }
    }
}
#endif	/* GRAFO_H */

