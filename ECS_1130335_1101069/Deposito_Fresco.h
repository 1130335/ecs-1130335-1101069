#ifndef DEPOSITO_FRESCO_H
#define	DEPOSITO_FRESCO_H

#include "PaleteFresco.h"
#include "Deposito.h"

class Deposito_Fresco : public Deposito {
private:
    vector<PaleteFresco> deposito; // contentor onde as paletes de um depósito fresco são guardadas
    vector <PaleteFresco> ::iterator m_itr; // iterador para inserção de produtos
    int m_atual; // palete atual onde a próxima inserção será feita
    vector<PaleteFresco> ::iterator m_it; // iterador para a remoção de produtos
    int m_att; // palete atual onde a próxima remoção será feita

public:
    Deposito_Fresco(); // construtor do depósito sem parâmetros
    Deposito_Fresco(int, int, int, double, int, int, int, int); // construtor do depósito com parâmetros
    Deposito_Fresco(const Deposito_Fresco&); // construtor que copia os atributos de um dado depósito para um novo
    ~Deposito_Fresco(); // destrutor
    void inserirProduto(string); // introduz um produto (assumido como string) numa palete do depósito
    void removerProduto(); // remove um produto da palete do depósito
    void escreve(); // mostra para o ecrã toda a informção sobre as paletes do depósito
    void setCapacidade(int); // altera a capacidade máxima das paletes do depósito
    void guardarFicheiro(ofstream&); // guarda a estrutura do depósito num ficheiro de texto

};

Deposito_Fresco::Deposito_Fresco() : Deposito() {
    for (int i = 0; i < Deposito::getPaletes(); i++) {
        deposito.push_back(PaleteFresco(Deposito::getCapacidade()));
    }
    m_itr = deposito.begin();
    m_atual = 0;
    m_it = deposito.begin();
    m_att = 0;

}

Deposito_Fresco::Deposito_Fresco(int chave, int n, int capacidade, double area,  int dep1, int dist1, int dep2, int dist2) : Deposito(chave, n, capacidade, area, dep1, dist1, dep2, dist2) {
    for (int i = 0; i < Deposito::getPaletes(); i++) {
        deposito.push_back(PaleteFresco(Deposito::getCapacidade()));
    }
    m_itr = deposito.begin();
    m_atual = 0;
    m_it = deposito.begin();
    m_att = 0;
}

Deposito_Fresco::Deposito_Fresco(const Deposito_Fresco& df) : Deposito(df) {
    this->deposito = df.deposito;
    this->m_itr = df.m_itr;
    this->m_atual = df.m_atual;
    this->m_it = df.m_it;
    this->m_att = df.m_att;
}

Deposito_Fresco::~Deposito_Fresco() {

}

void Deposito_Fresco::setCapacidade(int cap) {
    Deposito::setCapacidade(cap);
    vector<PaleteFresco> ::iterator it;
    for (it = deposito.begin(); it != deposito.end(); it++) {
        (*it).setCapacidade(cap);
    }
}

void Deposito_Fresco::inserirProduto(string produto) {
    if (m_atual == Deposito::getPaletes()) {
        m_itr = deposito.begin();
        m_atual = 0;
    }

    if ((*m_itr).getAtual()>(*(m_itr + 1)).getAtual()) {
        m_itr++;
        (*m_itr).inserirProduto(produto);
        m_atual++;
    } else if ((*m_itr).getAtual() == (*(m_itr + 1)).getAtual()) {
        if ((*m_itr).getAtual() == (*m_itr).getCapacidade()) {
            cout << "Erro: depósito cheio..." << endl;
        } else {
            (*m_itr).inserirProduto(produto);
            m_atual++;
        }
    }
}

void Deposito_Fresco::removerProduto() {
    if (m_att == Deposito::getPaletes()) {
        m_it = deposito.begin();
        m_att = 0;
    }

    if ((*m_it).getAtual()>(*(m_it + 1)).getAtual()) {
        (*m_it).removerProduto();
    } else if ((*m_it).getAtual() == (*(m_it + 1)).getAtual()) {
        if ((*m_it).getAtual() == 0) {
            cout << "Erro: depósito vazio..." << endl;
        } else {
            (*m_it).removerProduto();
            m_att++;
        }
    } else if ((*m_it).getAtual()<(*(m_it + 1)).getAtual()) {
        m_it++;
        (*m_it).removerProduto();
        m_att++;
    }
}

void Deposito_Fresco::escreve() {
    cout << " - FRESCO" << endl;
    Deposito::escreve();
    int cont = 1;
    vector<PaleteFresco> aux = deposito;
    vector<PaleteFresco> ::iterator it;
    if (this->getPaletes() == 0) {
        cout << "vazio" << endl;
    } else {
        for (it = aux.begin(); it != aux.end(); it++) {
            cout << "PALETE " << cont << endl;
            (*it).escreve();
            cont++;
            cout << endl;
        }
    }
}

void Deposito_Fresco::guardarFicheiro(ofstream& doc) {
    doc << " - FRESCO" << endl;
    Deposito::guardarFicheiro(doc);
    int cont = 1;
    vector<PaleteFresco> aux = deposito;
    vector<PaleteFresco> ::iterator it;
    if (this->getPaletes() == 0) {
        doc << "vazio" << endl;
    } else {
        for (it = aux.begin(); it != aux.end(); it++) {
            doc << "PALETE " << cont << endl;
            (*it).guardarFicheiro(doc);
            cont++;
            doc << endl;
        }
    }
}

#endif	/* DEPOSITO_FRESCO_H */

